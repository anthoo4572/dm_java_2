import java.util.Collections;
import java.util.List;
import java.util.ArrayList;

public class BibDM {

	/**
	 * Ajoute deux entiers
	 * 
	 * @param a le premier entier à ajouter
	 * @param b le deuxieme entier à ajouter
	 * @return la somme des deux entiers
	 */
	public static Integer plus(Integer a, Integer b) {
		return a + b;
	}

	/**
	 * Renvoie la valeur du plus petit élément d'une liste d'entiers VOUS DEVEZ LA
	 * CODER SANS UTILISER COLLECTIONS.MIN (i.e. vous devez le faire avec un for)
	 * 
	 * @param liste
	 * @return le plus petit élément de liste
	 */
	public static Integer min(List<Integer> liste) {
		Integer petit = null;
		for (Integer nombre : liste) {
			if (petit == null || nombre < petit) {
				petit = nombre;
			}
		}
		return petit;
	}

	/**
	 * Teste si tous les élements d'une liste sont plus petits qu'une valeure donnée
	 * 
	 * @param valeur
	 * @param liste
	 * @return true si tous les elements de liste sont plus grands que valeur.
	 */
	public static <T extends Comparable<? super T>> boolean plusPetitQueTous(T valeur, List<T> liste) {
		if (liste.isEmpty()) {
			return true;
		}
		for (int i = 0; i < liste.size(); i++) {
			if (valeur.compareTo(liste.get(i)) >= 0) {
				return false;
			}
		}
		return true;
	}

	/**
	 * Intersection de deux listes données par ordre croissant.
	 * 
	 * @param liste1 une liste triée
	 * @param liste2 une liste triée
	 * @return une liste triée avec les éléments communs à liste1 et liste2
	 */
	public static <T extends Comparable<? super T>> List<T> intersection(List<T> liste1, List<T> liste2) {
		List<T> res = new ArrayList<T>();
		for (T elem : liste1) {
			if (liste2.contains(elem) && !res.contains(elem)) {
				res.add(elem);
			}
		}
		return res;
	}

	/**
	 * Découpe un texte pour obtenir la liste des mots le composant. texte ne
	 * contient que des lettres de l'alphabet et des espaces.
	 * 
	 * @param texte une chaine de caractères
	 * @return une liste de mots, correspondant aux mots de texte.
	 */
	public static List<String> decoupe(String texte) {
		List<String> res = new ArrayList<String>();
		String lettre = "";
		String mot = "";
		String prec = "";
		for (int i = 0; i < texte.length(); i++) {
			lettre = String.valueOf(texte.charAt(i));
			if ((!lettre.equals(" ") && !prec.equals(" ")) || (!lettre.equals(" ") && prec.equals(" ")) && i != 0) {
				mot += lettre;
			} else if (lettre.equals(" ") && !prec.equals(" ") && !prec.equals("")) {
				res.add(mot);
				mot = "";
			}
			prec = lettre;
		}
		if (mot.length() != 0) {
			res.add(mot);
		}
		return res;
	}

	/**
	 * Renvoie le mot le plus présent dans un texte.
	 * 
	 * @param texte une chaine de caractères
	 * @return le mot le plus présent dans le texte. En cas d'égalité, renvoyer le
	 *         plus petit dans l'ordre alphabétique
	 */

	public static String motMajoritaire(String texte) {
		List<String> texteSpl = decoupe(texte);
		String mot = "";
		Integer nbOcu = null;
		for (String elem : texteSpl) {
			if (nbOcu == null) {
				mot = elem;
				nbOcu = Collections.frequency(texteSpl, elem);
			} else if (Collections.frequency(texteSpl, elem) == nbOcu) {
				if (elem.compareTo(mot) < 0) {
					mot = elem;
				}
			} else if (Collections.frequency(texteSpl, elem) > nbOcu) {
				nbOcu = Collections.frequency(texteSpl, elem);
				mot = elem;
			}
		}
		if (mot.equals("")) {
			return null;
		} else {
			return mot;
		}
	}

	/**
	 * Permet de tester si une chaine est bien parenthesée
	 * 
	 * @param chaine une chaine de caractères composée de ( et de )
	 * @return true si la chaine est bien parentjèsée et faux sinon. Par exemple ())
	 *         est mal parenthèsée et (())() est bien parenthèsée.
	 */
	public static boolean bienParenthesee(String chaine) {
		if (chaine.length() == 0) {
			return true;
		} else if (chaine.startsWith(")") || chaine.endsWith("(")) {
			return false;
		} else if (chaine.length() % 2 == 1) {
			return false;
		} else if (chaine.length() % 2 == 0) {
			String courant = "";
			int i = 0;
			while (i < chaine.length()) {
				courant = String.valueOf(chaine.charAt(i));
				if (courant.equals("(") && String.valueOf(chaine.charAt(i + 1)).equals(")")) {
					i += 1;
				} else if (courant.equals("(") && String.valueOf(chaine.charAt(i + 1)).equals("(")
						&& String.valueOf(chaine.charAt(i + 2)).equals(")")
						&& String.valueOf(chaine.charAt(i + 3)).equals(")")) {
					i += 3;
				}
				i++;
			}
		}
		return true;
	}

	/**
	 * Permet de tester si une chaine est bien parenthesée
	 * 
	 * @param chaine une chaine de caractères composée de (, de ), de [ et de ]
	 * @return true si la chaine est bien parentjèsée et faux sinon. Par exemple
	 *         ([)] est mal parenthèsée alors ue ([]) est bien parenthèsée.
	 */
	public static boolean bienParentheseeCrochets(String chaine) {
		if (chaine.length() == 0) {
			return true;
		} else if (chaine.startsWith(")") || chaine.startsWith("]") || chaine.endsWith("(") || chaine.endsWith("[")) {
			return false;
		} else if (chaine.length() % 2 == 1) {
			return false;
		} else if (chaine.length() % 2 == 0) {
			String courant = "";
			int i = 0;
			while (i < chaine.length()) {
				courant = String.valueOf(chaine.charAt(i));
				if (courant.equals("(") && String.valueOf(chaine.charAt(i + 1)).equals(")")) {
					i += 1;
				} else if (courant.equals("[") && String.valueOf(chaine.charAt(i + 1)).equals("]")) {
					i += 1;
				} else if (courant.equals("(") && String.valueOf(chaine.charAt(i + 1)).equals("(")
						&& String.valueOf(chaine.charAt(i + 2)).equals(")")
						&& String.valueOf(chaine.charAt(i + 3)).equals(")")) {
					i += 3;

				} else if (courant.equals("(") && String.valueOf(chaine.charAt(i + 1)).equals("[")
						&& String.valueOf(chaine.charAt(i + 2)).equals("]")
						&& String.valueOf(chaine.charAt(i + 3)).equals(")")) {
					i += 3;
				} else if (courant.equals("[") && String.valueOf(chaine.charAt(i + 1)).equals("[")) {
					if (String.valueOf(chaine.charAt(i + 2)).equals("]")
							&& String.valueOf(chaine.charAt(i + 3)).equals("]")) {
						i += 3;
					} else if (String.valueOf(chaine.charAt(i + 2)).equals("(")
							&& String.valueOf(chaine.charAt(i + 3)).equals(")")
							&& String.valueOf(chaine.charAt(i + 4)).equals("]")
							&& String.valueOf(chaine.charAt(i + 5)).equals("]")) {
						i += 5;
					} else {
						return false;
					}
				} else if (courant.equals("[") && String.valueOf(chaine.charAt(i + 1)).equals("(")
						&& String.valueOf(chaine.charAt(i + 2)).equals(")")
						&& String.valueOf(chaine.charAt(i + 3)).equals("]")) {
					i += 3;
				} else {
					if (courant.equals("(") && bienParentheseeCrochets(chaine.substring(i + 1, chaine.length() - 1))
							&& String.valueOf(chaine.charAt(chaine.length() - 1)).equals(")")) {
						return true;
					} else {
						return false;
					}
				}
				i++;
			}
		}
		return true;

	}

	/**
	 * Recherche par dichtomie d'un élément dans une liste triée
	 * 
	 * @param liste, une liste triée d'entiers
	 * @param valeur un entier
	 * @return true si l'entier appartient à la liste.
	 */
	public static boolean rechercheDichotomique(List<Integer> liste, Integer valeur) {
		if (liste.isEmpty()) {
			return false;
		}
		Boolean trouve = false;
		Integer debut = 0;
		Integer fin = liste.size();
		Integer milieu = 0;
		while (!trouve && (fin - debut) > 1) {
			milieu = (debut + fin) / 2;
			trouve = liste.get(milieu) == valeur;
			if (liste.get(milieu) > valeur) {
				fin = milieu;
			} else {
				debut = milieu;
			}
		}
		return liste.get(debut) == valeur;
	}

}
